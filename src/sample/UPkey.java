package sample;

public class UPkey{
    public static double setxy(double x, double y) {
        if (x > 0 && x <= 45 && y > 10 && y < 40) { //A1
            return 0;
        } else if (x >= 41 && x <= 106 && y >= -20 && y <= 90) {//A2+A3
            return 0;
        } else if (x > 140 && x <= 227 && y > -130 && y <= -60) {//B1
            return 0;
        } else if (x > 190 && x < 259 && y > -116 && y <= 41) {//B2+B3
            return 0;
        } else if (x > 292 && x < 357 && y > -100 && y <= 39) {//C1+C2+C3
            return 0;
        } else if (x > 339 && x < 407 && y > -36 && y <= 141) {//C4+C5+C6+C7
            return 0;
        } else if (x > 330 && x < 507 && y > -80 && y <= -9) {//C8
            return 0;
        } else if (x > 384 && x < 608 && y > 22 && y <= 89) {//C9
            return 0;
        } else if (x > 589 && x < 707 && y > -75 && y <= -10) {//D3+D4
            return 0;
        } else if (x > 670 && x < 758 && y > -175 && y <= -110) {//D2
            return 0;
        } else if (x > 0 && x <= 56 && y < -109) {   //E
            return 0;
        } else if (x > 0 && x <= 256 && y <= -161) { //F
            return 0;
        } else if (x > 290 && x <= 505 && y <= -160) { //G
            return 0;
        } else if (x >= 791 && y > -171 && y <= -60) { //H
            return 0;
        } else if (x > 690 && y > 7 && y <= 91) { //I
            return 0;
        } else if (x > 590 && x < 708 && y >= 120 && y <= 188) { //L
            return 0;
        } else if (x > 738 && x < 806 && y >= 120 && y <= 188) { //M
            return 0;
        } else if (y <= -210) {
            return 0;
        }
        return 1;
    }
}