package sample;

public class LEFTkey {
    public static double setxy(double x, double y) {
        if (x > 70 && x <= 112 && y >= -39 && y < 90) {//A2+A3
            return 0;
        } else if (x >= 210 && x <= 260 && y > -142 && y < 41) {//B2+B3
            return 0;
        } else if (x > 317 && x <= 360 && y > -140 && y < -42) {//C1
            return 0;
        } else if (x > 317 && x <= 511 && y > -91 && y < -9) {//C2+C4+C8
            return 0;
        } else if (x > 300 && x <= 411 && y > -31 && y < 23) {//C3+C5
            return 0;
        } else if (x > 380 && x <= 608 && y > 10 && y < 89) {//C6+C9
            return 0;
        } else if (x > 367 && x <= 407 && y > 67 && y < 141) {//C7
            return 0;
        } else if (x > 670 && x <= 710 && y < -10) {//D1+D4
            return 0;
        } else if (x > 670 && x <= 761 && y > -190 && y < -110) {//D2
            return 0;
        } else if (x <= 61 && y > -200 && y < -110) {//E
            return 0;
        } else if (x <= 261 && y < -161) {//F
            return 0;
        } else if (x > 335 && x <= 511 && y < -161) {//G
            return 0;
        } else if (x >= 270 && x <= 311 && y >= 110) {//J
            return 0;
        } else if (x >= 430 && x <= 561 && y > 161) {//K
            return 0;
        } else if (x > 620 && x <= 710 && y > 107 && y < 188) {//L
            return 0;
        } else if (x > 775 && x <= 810 && y > 107 && y < 188) {//M
            return 0;
        } else if (x <= 11) {
            return 0;
        }
        return 1;
    }
}