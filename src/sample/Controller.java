package sample;

import javafx.animation.AnimationTimer;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public Label txtTest;
    public Button btn_restart;

    @FXML
    public MediaView MediaStart;
    public MediaView endMediaview;

    Media media, endMedia;
    MediaPlayer mediaplayer, endMediaplayer;

    double x = 30, y = 200;
    double GEx = 430, GEy = 0, PEx = 80, PEy = -150, YEx = 430, YEy = 100;
    double Sx = 0, Sy = 0;
    int direction, GEdirection = 0, PEdirection = 0, YEdirection = 0, PauseStatus = 0;
    int touchG = 0, touchP = 0, touchY = 0;

    @FXML
    public ImageView Background;

    @FXML
    public ImageView StartBackGround;

    public ImageView LeadRole;
    public ImageView Skill;
    public ImageView GEnemy;
    public ImageView PEnemy;
    public ImageView YEnemy;

    public Button btn_start;
    public Button btn_End;
    @FXML
    public BorderPane borderPane;

    public GridPane gridPane;

    int plotX = 0;

    Boolean GoRight = false;
    Boolean GoLeft = false;
    Boolean GoUp = false;
    Boolean GoDown = false;
    Boolean DoAttack = false;
    Boolean PressKey = false;
    Boolean GELife = true, PELife = true, YELife = true, ifstart = false;
    Task GEAct, PEAct, YEAct;
    Task task2;
    Task Over;
    Task AtkOver;
    Task Play;

    private int count = 0;

    //private final Text text = new Text(Integer.toString(count));
    private void incrementCount() {
        count++;
        // text.setTextContent(Integer.toString(count));
    }

    Runnable updater = new Runnable() {

        @Override
        public void run() {
            incrementCount();
        }
    };

    @FXML
    public void handleKeyPress(KeyEvent event) {
        if (PauseStatus == 0) {
            if (PressKey == false && (event.getCode() == KeyCode.W || event.getCode() == KeyCode.UP)) {
                GoUp = true;
                PressKey = true;
                direction = 1;
                //txtTest.setText("UP ON");
            }
            if (PressKey == false && (event.getCode() == KeyCode.A || event.getCode() == KeyCode.LEFT)) {
                GoLeft = true;
                PressKey = true;
                direction = 2;
                //txtTest.setText("LEFT ON");
                //LeadRole.setImage(new Image(getClass().getResource("/resources/AquAGoRight.png").toString()));

            }
            if (PressKey == false && (event.getCode() == KeyCode.S || event.getCode() == KeyCode.DOWN)) {
                GoDown = true;
                PressKey = true;
                direction = 3;
                //txtTest.setText("DOWN ON");
                //LeadRole.setImage(new Image(getClass().getResource("/resources/AquAGoRight.png").toString()));

            }
            if (PressKey == false && (event.getCode() == KeyCode.D || event.getCode() == KeyCode.RIGHT)) {
                GoRight = true;
                PressKey = true;
                direction = 4;
                //txtTest.setText("RIGHT ON");
                // LeadRole.setImage(new Image(getClass().getResource("/resources/AquAGoRight.png").toString()));

            }
            if (PressKey == false && (event.getCode() == KeyCode.ESCAPE || event.getCode() == KeyCode.P)) {
                /******************************暫停*********************************/
                PressKey = true;
                GEAct.cancel();
                PauseStatus = 1;
                if (GEdirection == 1) {
                    GEdirection = 2;
                }
                // btn_restart.setVisible(true);
                //\\ btn_start.setTranslateY(0);
                btn_start.setVisible(true);

            }
            if (PressKey == false && (event.getCode() == KeyCode.SPACE || event.getCode() == KeyCode.J)) {
                DoAttack = true;
                PressKey = true;
                /*******************攻擊判斷*************************/
                AtkOver = new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        while (true) {
                            Thread.sleep(10);
                            Sx = Skill.getTranslateX();
                            Sy = Skill.getTranslateY();
                            if (((((Sx - 20) <= (GEx + 20) && (Sy + 20) <= (GEy + 13) && (Sy + 7) >= (GEy - 13)) ||
                                    ((Sx - 20) <= (GEx + 20) && (Sy + 7) <= (GEy + 13) && (Sy - 7) >= (GEy - 13)) ||
                                    ((Sx - 20) <= (GEx + 20) && (Sy - 7) <= (GEy + 13) && (Sy - 20) >= (GEy - 13))) &&
                                    ((Sx >= (GEx - 20) && (Sy + 7) <= (GEy + 13) && (Sy - 7) >= (GEy - 13)) ||
                                            (Sx >= (GEx - 20) && (Sy + 7) <= (GEy + 13) && (Sy - 7) >= (GEy - 13)) ||
                                            (Sx >= (GEx - 20) && (Sy - 7) <= (GEy + 13) && (Sy - 20) >= (GEy - 13)))) ||
                                    /*********************************中線*****************************/
                                    ((((Sx + 20) >= (GEx - 20) && (Sy + 20) <= (GEy + 13) && (Sy + 7) >= (GEy - 13)) ||
                                            ((Sx + 20) >= (GEx - 20) && (Sy + 7) <= (GEy + 13) && (Sy - 7) >= (GEy - 13)) ||
                                            ((Sx + 20) >= (GEx - 20) && (Sy - 7) <= (GEy + 13) && (Sy - 20) >= (GEy - 13))) &&
                                            ((Sx <= (GEx + 20) && (Sy + 7) <= (GEy + 13) && (Sy - 7) >= (GEy - 13)) ||
                                                    (Sx <= (GEx + 20) && (Sy + 7) <= (GEy + 13) && (Sy - 7) >= (GEy - 13)) ||
                                                    (Sx <= (GEx + 20) && (Sy - 7) <= (GEy + 13) && (Sy - 20) >= (GEy - 13))))) {
                                GEAct.cancel();
                                Thread.sleep(10);
                                GEnemy.setTranslateX(GEx = 0);
                                GEnemy.setTranslateY(GEy = 0);
                                GEnemy.setVisible(false);
                                GELife = false;
                            }
                            /****************************************************************/
                            /*if (((((Sx - 20) <= (PEx + 20) && (Sy + 20) <= (PEy + 13) && (Sy + 7) >= (PEy - 13)) ||
                                    ((Sx - 20) <= (PEx + 20) && (Sy + 7) <= (PEy + 13) && (Sy - 7) >= (PEy - 13)) ||
                                    ((Sx - 20) <= (PEx + 20) && (Sy - 7) <= (PEy + 13) && (Sy - 20) >= (PEy - 13))) &&
                                    ((Sx >= (PEx - 20) && (Sy + 7) <= (PEy + 13) && (Sy - 7) >= (PEy - 13)) ||
                                            (Sx >= (PEx - 20) && (Sy + 7) <= (PEy + 13) && (Sy - 7) >= (PEy - 13)) ||
                                            (Sx >= (PEx - 20) && (Sy - 7) <= (PEy + 13) && (Sy - 20) >= (PEy - 13)))) ||*/
                            /*********************************中線*****************************/
                                    /*((((Sx + 20) >= (PEx - 20) && (Sy + 20) <= (PEy + 13) && (Sy + 7) >= (PEy - 13)) ||
                                            ((Sx + 20) >= (PEx - 20) && (Sy + 7) <= (PEy + 13) && (Sy - 7) >= (PEy - 13)) ||
                                            ((Sx + 20) >= (PEx - 20) && (Sy - 7) <= (PEy + 13) && (Sy - 20) >= (PEy - 13))) &&
                                            ((Sx <= (PEx + 20) && (Sy + 7) <= (PEy + 13) && (Sy - 7) >= (PEy - 13)) ||
                                                    (Sx <= (PEx + 20) && (Sy + 7) <= (PEy + 13) && (Sy - 7) >= (PEy - 13)) ||
                                                    (Sx <= (PEx + 20) && (Sy - 7) <= (PEy + 13) && (Sy - 20) >= (PEy - 13))))) {
                                PEAct.cancel();
                                Thread.sleep(10);
                                PEnemy.setTranslateX(PEx = 0);
                                PEnemy.setTranslateY(PEy = 0);
                                PEnemy.setVisible(false);
                                PELife = false;
                            }*/

                        }
                    }
                };
                new Thread(AtkOver).start();
                /********************************************************************/
                //txtTest.setText("SPACE ON");
                //LeadRole.setImage(new Image(getClass().getResource("/resources/AquAUpRight.png").toString()));
            }

            //timer.start();
        }
    }

    public void handleKeyReleased(KeyEvent event) throws InterruptedException {
        if (event.getCode() == KeyCode.D || event.getCode() == KeyCode.RIGHT) {
            Thread.sleep(10);
            GoRight = false;
            PressKey = false;
            direction = 4;
            task2.cancel();
            //txtTest.setText("T");
            LeadRole.setImage(new Image(getClass().getResource("/resources/AquAStandRight.png").toString()));
            //System.out.println("RIGHT_release");
        }
        if (event.getCode() == KeyCode.A || event.getCode() == KeyCode.LEFT) {
            Thread.sleep(10);
            GoLeft = false;
            PressKey = false;
            direction = 2;
            task2.cancel();
            //txtTest.setText("T");
            LeadRole.setImage(new Image(getClass().getResource("/resources/AquAStandLeft.png").toString()));
            //System.out.println("LEFT_release");
        }
        if (event.getCode() == KeyCode.W || event.getCode() == KeyCode.UP) {
            Thread.sleep(10);
            GoUp = false;
            PressKey = false;
            direction = 1;
            task2.cancel();
            //txtTest.setText("T");
            LeadRole.setImage(new Image(getClass().getResource("/resources/AquAStandUp.png").toString()));
            //System.out.println("LEFT_release");
        }
        if (event.getCode() == KeyCode.S || event.getCode() == KeyCode.DOWN) {
            Thread.sleep(10);
            GoDown = false;
            PressKey = false;
            direction = 3;
            task2.cancel();
            //txtTest.setText("T");
            LeadRole.setImage(new Image(getClass().getResource("/resources/AquAStandRight.png").toString()));
            //System.out.println("LEFT_release");
        }
        if (event.getCode() == KeyCode.SPACE || event.getCode() == KeyCode.J) {
            Thread.sleep(10);
            DoAttack = false;
            PressKey = false;
            //txtTest.setText("T");
            AtkOver.cancel();
            if (direction == 1) {
                LeadRole.setImage(new Image(getClass().getResource("/resources/AquAStandUp.png").toString()));
            } else if (direction == 2) {
                LeadRole.setImage(new Image(getClass().getResource("/resources/AquAStandLeft.png").toString()));
            } else if (direction == 3) {
                LeadRole.setImage(new Image(getClass().getResource("/resources/AquAStandRight.png").toString()));
            } else if (direction == 4) {
                LeadRole.setImage(new Image(getClass().getResource("/resources/AquAStandRight.png").toString()));
            }
            Skill.setVisible(false);
        }
    }

    AnimationTimer animationTimer = new AnimationTimer() {

        @Override
        public void handle(long now) {
            task2 = new Task<Void>() {

                @Override
                protected Void call() throws Exception {
                    if (GoLeft == true) {
                        Task task3 = new Task<Void>() {

                            @Override
                            protected Void call() throws Exception {
                                LeadRole.setImage(new Image(getClass().getResource("/resources/AquAGoLeft.png").toString()));
                                Thread.sleep(100);
                                LeadRole.setImage(new Image(getClass().getResource("/resources/AquAStandLeft.png").toString()));
                                Thread.sleep(100);
                                return null;
                            }
                        };
                        new Thread(task3).start();
                        if (LEFTkey.setxy(x, y) == 1) {
                            LeadRole.setTranslateX(x -= 1);
                            //System.out.println(LeadRole.getTranslateX() + " " + LeadRole.getTranslateY());
                        }
                        Thread.sleep(1000);
                        //LeadRole.setImage(new Image(getClass().getResource("/resources/AquAStandRight.png").toString()));
                    } else if (GoRight == true) {
                        Task task3 = new Task<Void>() {

                            @Override
                            protected Void call() throws Exception {
                                LeadRole.setImage(new Image(getClass().getResource("/resources/AquAGoRight.png").toString()));
                                Thread.sleep(100);
                                LeadRole.setImage(new Image(getClass().getResource("/resources/AquAStandRight.png").toString()));
                                Thread.sleep(100);
                                return null;
                            }
                        };
                        new Thread(task3).start();
                        if (RIGHTkey.setxy(x, y) == 1) {
                            LeadRole.setTranslateX(x += 1);
                            //System.out.println(LeadRole.getTranslateX() + " " + LeadRole.getTranslateY());
                        }
                        Thread.sleep(1000);
                        //LeadRole.setImage(new Image(getClass().getResource("/resources/Stand_to_Go.gif").toString()));
                    } else if (GoUp == true) {
                        Task task3 = new Task<Void>() {

                            @Override
                            protected Void call() throws Exception {
                                LeadRole.setImage(new Image(getClass().getResource("/resources/AquAGoUp.png").toString()));
                                Thread.sleep(100);
                                LeadRole.setImage(new Image(getClass().getResource("/resources/AquAStandUp.png").toString()));
                                Thread.sleep(100);
                                return null;
                            }
                        };
                        new Thread(task3).start();
                        if (UPkey.setxy(x, y) == 1) {
                            LeadRole.setTranslateY(y -= 1);
                            //System.out.println(LeadRole.getTranslateX() + " " + LeadRole.getTranslateY());
                        }
                        Thread.sleep(1000);
                        //LeadRole.setImage(new Image(getClass().getResource("/resources/Stand_to_Go.gif").toString()));
                    } else if (GoDown == true) {
                        Task task3 = new Task<Void>() {

                            @Override
                            protected Void call() throws Exception {
                                LeadRole.setImage(new Image(getClass().getResource("/resources/AquAGoRight.png").toString()));
                                Thread.sleep(100);
                                LeadRole.setImage(new Image(getClass().getResource("/resources/AquAStandRight.png").toString()));
                                Thread.sleep(100);
                                return null;
                            }
                        };
                        new Thread(task3).start();
                        if (DOWNkey.setxy(x, y) == 1) {
                            LeadRole.setTranslateY(y += 1);
                            //System.out.println(LeadRole.getTranslateX() + " " + LeadRole.getTranslateY());
                        }
                        Thread.sleep(1000);
                        //LeadRole.setImage(new Image(getClass().getResource("/resources/Stand_to_Go.gif").toString()));
                    } else if (DoAttack == true) {
                        /**********************攻擊*****************************/
                        Skill.setVisible(true);
                        if (direction == 1) {
                            LeadRole.setImage(new Image(getClass().getResource("/resources/AquAUpUp.png").toString()));
                            Skill.setTranslateX(x);
                            Skill.setTranslateY(y - 35);
                            Thread.sleep(10);
                            Task task3 = new Task<Void>() {

                                @Override
                                protected Void call() throws Exception {
                                    Skill.setImage(new Image(getClass().getResource("/resources/Skill_1.png").toString()));
                                    Thread.sleep(100);
                                    Skill.setImage(new Image(getClass().getResource("/resources/Skill_2.png").toString()));
                                    Thread.sleep(100);
                                    return null;
                                }
                            };
                            new Thread(task3).start();
                        } else if (direction == 2) {
                            LeadRole.setImage(new Image(getClass().getResource("/resources/AquAUpLeft.png").toString()));
                            Skill.setTranslateX(x - 35);
                            Skill.setTranslateY(y);
                            Thread.sleep(10);
                            Task task3 = new Task<Void>() {

                                @Override
                                protected Void call() throws Exception {
                                    Skill.setImage(new Image(getClass().getResource("/resources/Skill_1.png").toString()));
                                    Thread.sleep(100);
                                    Skill.setImage(new Image(getClass().getResource("/resources/Skill_2.png").toString()));
                                    Thread.sleep(100);
                                    return null;
                                }
                            };
                            new Thread(task3).start();
                        } else if (direction == 3) {
                            LeadRole.setImage(new Image(getClass().getResource("/resources/AquAUpRight.png").toString()));
                            Skill.setTranslateX(x);
                            Skill.setTranslateY(y + 35);
                            Thread.sleep(10);
                            Task task3 = new Task<Void>() {

                                @Override
                                protected Void call() throws Exception {
                                    Skill.setImage(new Image(getClass().getResource("/resources/Skill_1.png").toString()));
                                    Thread.sleep(100);
                                    Skill.setImage(new Image(getClass().getResource("/resources/Skill_2.png").toString()));
                                    Thread.sleep(100);
                                    return null;
                                }
                            };
                            new Thread(task3).start();
                        } else if (direction == 4) {
                            LeadRole.setImage(new Image(getClass().getResource("/resources/AquAUpRight.png").toString()));
                            Skill.setTranslateX(x + 35);
                            Skill.setTranslateY(y);
                            Thread.sleep(10);
                            Task task3 = new Task<Void>() {

                                @Override
                                protected Void call() throws Exception {
                                    Skill.setImage(new Image(getClass().getResource("/resources/Skill_1.png").toString()));
                                    Thread.sleep(100);
                                    Skill.setImage(new Image(getClass().getResource("/resources/Skill_2.png").toString()));
                                    Thread.sleep(100);
                                    return null;
                                }
                            };
                            new Thread(task3).start();
                        }
                        Thread.sleep(1000);
                    }
                    return null;
                }
                /*@Override
                protected void scheduled(){
                    super.scheduled();
                }

                @Override
                protected void succeeded(){
                    super.succeeded();
                }*/

            };

            //if((x+=10 == GEx||)

            new Thread(task2).start();

        }

    };


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //File file = new File("D:\\user\\Desktop\\FinalReport_All\\Picture\\Final_Report_Background_2.jpg");
        LeadRole.setImage(new Image(getClass().getResource("/resources/AquAStandRight.png").toString()));
        Background.setImage(new Image(getClass().getResource("/resources/Final_Report_Background.jpg").toString()));
        StartBackGround.setImage(new Image(getClass().getResource("/resources/StartExplosion.jpg").toString()));
        StartBackGround.fitHeightProperty().set(510);
        StartBackGround.fitWidthProperty().set(900);

        /*********MP4封面******/
        Play = new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                String MediaFile = new File("src/resources/MainExplosion.mp4").getAbsolutePath();
                media = new Media(new File(MediaFile).toURI().toString());
                mediaplayer = new MediaPlayer(media);
                //MediaStart = new MediaView(mediaplayer);
                MediaStart.fitHeightProperty().set(590);
                MediaStart.fitWidthProperty().set(990);
                MediaStart.setMediaPlayer(mediaplayer);
                mediaplayer.play();
                for (int i = 0; ifstart != true && i < 135; i += 1) {
                    Thread.sleep(100);
                }
                mediaplayer.stop();
                MediaStart.setVisible(false);
                Thread.sleep(2000);
                MediaFile = new File("src/resources/MondayAfternoon.mp3").getAbsolutePath();
                media = new Media(new File(MediaFile).toURI().toString());
                mediaplayer = new MediaPlayer(media);
                MediaStart = new MediaView(mediaplayer);
                mediaplayer.play();
                // borderPane.getChildren().removeAll();
                return null;

            }
        };
        /****************************/
        new Thread(Play).start();
        //----------------------------------------------------------------------------------------------------
        /*List<String> imagepath = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            imagepath.add("/resources/".concat(String.valueOf(i)).concat(".png"));
        }
        Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);*/


        //MediaStart.setVisible(false);


    }

    @FXML
    public void doStart(ActionEvent actionEvent) {
        ifstart = true;
        StartBackGround.setVisible(false);
        btn_start.setVisible(false);
        btn_restart.setVisible(false);
        btn_End.setVisible(false);
        //btn_End.setVisible(false);
        animationTimer.start();
        PauseStatus = 0;

        //pbWorking.progressProperty().bind(task.progressProperty());
        //pbWorking.setVisible(true);
        /******************綠青蛙************************/
        if (GELife == true) {
            GEAct = new Task<Void>() {

                @Override
                protected Void call() throws Exception {
                    final int max = 670, min = 430;
                    while (true) {
                        for (; GEx < max; ) {
                            if (GEdirection == 2) {
                                break;
                            }
                            GEdirection = 0;
                            GEnemy.setTranslateX(GEx += 3);
                            GEnemy.setImage(new Image(getClass().getResource("/resources/GWa_JumpR.png").toString()));
                            Thread.sleep(100);
                            GEnemy.setTranslateX(GEx += 3);
                            GEnemy.setImage(new Image(getClass().getResource("/resources/GWa_StandR.png").toString()));
                            Thread.sleep(100);
                        }
                        for (; GEx > min; ) {
                            GEdirection = 1;
                            GEnemy.setTranslateX(GEx -= 3);
                            GEnemy.setImage(new Image(getClass().getResource("/resources/GWa_Jump.png").toString()));
                            Thread.sleep(100);
                            GEnemy.setTranslateX(GEx -= 3);
                            GEnemy.setImage(new Image(getClass().getResource("/resources/GWa_Stand.png").toString()));
                            Thread.sleep(100);
                        }

                    }
                }

            };
        }

        if (PELife == true) {
            PEAct = new Task<Void>() {

                @Override
                protected Void call() throws Exception {
                    final int max = 620, min = 80;
                    while (true) {
                        for (; PEx < max; ) {
                            if (PEdirection == 2) {
                                break;
                            }
                            PEdirection = 0;
                            PEnemy.setTranslateX(PEx += 3);
                            PEnemy.setImage(new Image(getClass().getResource("/resources/PWa_JumpR.png").toString()));
                            Thread.sleep(100);
                            PEnemy.setTranslateX(PEx += 3);
                            PEnemy.setImage(new Image(getClass().getResource("/resources/PWa_StandR.png").toString()));
                            Thread.sleep(100);
                        }
                        for (; PEx > min; ) {
                            PEdirection = 1;
                            PEnemy.setTranslateX(PEx -= 3);
                            PEnemy.setImage(new Image(getClass().getResource("/resources/PWa_Jump.png").toString()));
                            Thread.sleep(100);
                            PEnemy.setTranslateX(PEx -= 3);
                            PEnemy.setImage(new Image(getClass().getResource("/resources/PWa_Stand.png").toString()));
                            Thread.sleep(100);
                        }

                    }
                }

            };
        }

        if (YELife == true) {
            YEAct = new Task<Void>() {

                @Override
                protected Void call() throws Exception {
                    final int max = 820, min = 430;
                    while (true) {
                        for (; YEx < max; ) {
                            if (YEdirection == 2) {
                                break;
                            }
                            YEdirection = 0;
                            YEnemy.setTranslateX(YEx += 3);
                            YEnemy.setImage(new Image(getClass().getResource("/resources/YWa_JumpR.png").toString()));
                            Thread.sleep(100);
                            YEnemy.setTranslateX(YEx += 3);
                            YEnemy.setImage(new Image(getClass().getResource("/resources/YWa_StandR.png").toString()));
                            Thread.sleep(100);
                        }
                        for (; YEx > min; ) {
                            YEdirection = 1;
                            YEnemy.setTranslateX(YEx -= 3);
                            YEnemy.setImage(new Image(getClass().getResource("/resources/YWa_Jump.png").toString()));
                            Thread.sleep(100);
                            YEnemy.setTranslateX(YEx -= 3);
                            YEnemy.setImage(new Image(getClass().getResource("/resources/YWa_Stand.png").toString()));
                            Thread.sleep(100);
                        }

                    }
                }

            };
        }
        /******************判斷是否碰到青蛙*************/
        Over = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                while (true) {
                    Thread.sleep(10);
                    /**********************綠青蛙***************************/
                    if ((((x - 10) <= (GEx + 20) && (y + 17) <= (GEy + 13) && (y + 6) >= (GEy - 13)) ||
                            ((x - 10) <= (GEx + 20) && (y - 6) >= (GEy - 13) && (y + 6) <= (GEy + 13)) ||
                            ((x - 10) <= (GEx + 20) && (y - 17) >= (GEy - 13) && (y - 6) <= (GEy + 13))) &&
                            (((x + 10) >= (GEx - 20) && (y + 17) <= (GEy + 13) && (y + 6) >= (GEy - 13)) ||
                                    ((x + 10) >= (GEx - 20) && (y - 6) >= (GEy - 13) && (y + 6) <= (GEy + 13)) ||
                                    ((x + 10) >= (GEx - 20) && (y - 17) >= (GEy - 13) && (y - 6) <= (GEy + 13)))) {
                        LeadRole.setVisible(false);
                        Thread.sleep(100);
                        LeadRole.setVisible(true);
                        touchG = 1;

                        GEAct.cancel();
                        Thread.sleep(10);
                        GEnemy.setTranslateX(GEx = 0);
                        GEnemy.setTranslateY(GEy = 0);
                        GEnemy.setVisible(false);
                        GELife = false;
                    }
                    /**********************粉青蛙***************************/
                    /*if ((((x - 10) <= (YEx + 20) && (y + 17) <= (YEy + 13) && (y + 6) >= (YEy - 13)) ||
                            ((x - 10) <= (YEx + 20) && (y - 6) >= (YEy - 13) && (y + 6) <= (YEy + 13)) ||
                            ((x - 10) <= (YEx + 20) && (y - 17) >= (YEy - 13) && (y - 6) <= (YEy + 13))) &&
                            (((x + 10) >= (YEx - 20) && (y + 17) <= (YEy + 13) && (y + 6) >= (YEy - 13)) ||
                                    ((x + 10) >= (YEx - 20) && (y - 6) >= (YEy - 13) && (y + 6) <= (YEy + 13)) ||
                                    ((x + 10) >= (YEx - 20) && (y - 17) >= (YEy - 13) && (y - 6) <= (YEy + 13)))) {
                        LeadRole.setVisible(false);
                        Thread.sleep(100);
                        LeadRole.setVisible(true);
                        touchY = 1;
                        YELife = false;
                    }*/
                    /**********************黃青蛙***************************/
                    /*if (x == 0) {

                    }*/
                    /*************************結尾判斷******************/
                    if (GELife == false) {
                        Play.cancel();
                        MediaStart.setVisible(true);
                        mediaplayer.stop();
                        if (touchG == 1) {
                            String MediaFile = new File("src/resources/MainGAeat.mp4").getAbsolutePath();
                            endMedia = new Media(new File(MediaFile).toURI().toString());
                            endMediaplayer = new MediaPlayer(endMedia);
                            endMediaview.fitHeightProperty().set(510);
                            endMediaview.fitWidthProperty().set(900);
                            endMediaview.setMediaPlayer(endMediaplayer);
                            //borderPane.getChildren().add(EndMedia);
                            endMediaplayer.play();
                            Thread.sleep(5200);
                            System.exit(1);

                        } else if (touchG == 0) {
                            String MediaFile = new File("src/resources/MainWin.mp4").getAbsolutePath();
                            endMedia = new Media(new File(MediaFile).toURI().toString());
                            endMediaplayer = new MediaPlayer(endMedia);
                            endMediaview.fitHeightProperty().set(510);
                            endMediaview.fitWidthProperty().set(900);
                            endMediaview.setMediaPlayer(endMediaplayer);
                            //borderPane.getChildren().add(EndMedia);
                            endMediaplayer.play();
                            Thread.sleep(15020);
                            System.exit(1);
                        }
                    }

                }
            }
        };

        new Thread(Over).start();
        new Thread(GEAct).start();
        //new Thread(PEAct).start();
        //new Thread(YEAct).start();

        borderPane.requestFocus();
    }

    public void doEnd(ActionEvent actionEvent) {
        System.exit(1);
    }

    public void doRestart(ActionEvent actionEvent) {
        x = 30;
        y = 200;
        LeadRole.setTranslateX(x);
        LeadRole.setTranslateY(y);
    }

}