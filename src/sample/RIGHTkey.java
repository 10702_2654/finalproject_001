package sample;

public class RIGHTkey {
    public static double setxy(double x, double y) {
        if (x >= 39 && x < 60 && y >= -35 && y <= 85) {//A3
            return 0;
        } else if (x >= 139 && x <= 227 && y > -142 && y < -60) {//B1+B2
            return 0;
        } else if (x >= 190 && x < 245 && y > -142 && y < 41) {//B3
            return 0;
        } else if (x >= 290 && x < 329 && y > -140 && y < 39) {//C1+C2+C3
            return 0;
        } else if (x >= 339 && x < 383 && y > 0 && y < 138) {//C6+C7
            return 0;
        } else if (x >= 639 && x < 695 && y < -40) {//D1
            return 0;
        } else if (x >= 589 && x < 695 && y > -90 && y < -10) {//D3+D4
            return 0;
        } else if (x >= 290 && x < 415  && y < -161) {//G
            return 0;
        } else if (x >= 790 && y > -192 && y <= -61) {//H
            return 0;
        } else if (x >= 690 && y > 7 && y < 91) {//I
            return 0;
        } else if (x >= 239 && x <= 285 && y >= 110) {//J
            return 0;
        } else if (x >= 390 && x < 527 && y > 161) {//K
            return 0;
        } else if (x >= 590 && x < 680 && y > 107 && y < 188) {//L
            return 0;
        } else if (x >= 738 && x < 785 && y > 107 && y < 188) {//M
            return 0;
        } else if (x >= 838) {
            return 0;
        }
        return 1;
    }
}