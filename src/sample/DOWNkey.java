package sample;

public class DOWNkey {
    public static double setxy(double x, double y) {
        if (x > 0 && x <= 106 && y >= -43 && y < -10) { //A1+A2
            return 0;
        } else if (x > 140 && x < 258 && y >= -143 && y < -107) { //B1+B2
            return 0;
        } else if (x > 292 && x < 357 && y >= -141 && y < -10) { //C1+C2+C3
            return 0;
        } else if (x > 336 && x < 507 && y >= -91 && y < -36) { //C4+C8
            return 0;
        } else if (x > 375 && x < 608 && y >= 10 && y < 70) { //C9
            return 0;
        } else if (x > 589 && x < 707 && y >= -90 && y < -20) { //D3
            return 0;
        } else if (x > 670 && x < 758 && y >= -191 && y < -130) { //D2
            return 0;
        } else if (x > 790 && y >= -192 && y < -105) { //H
            return 0;
        } else if (x > 690 && y >= 7 && y <= 80) { //I
            return 0;
        } else if (x > 241 && x < 308 && y >= 107) { //J
            return 0;
        } else if (x > 390 && x < 558 && y >= 161) { //K
            return 0;
        } else if (x > 590 && x < 708 && y >= 107 && y < 150) { //L
            return 0;
        } else if (x > 738 && x < 806 && y >= 107 && y < 150) { //M
            return 0;
        } else if (y >= 208) {
            return 0;
        }
        return 1;
    }
}